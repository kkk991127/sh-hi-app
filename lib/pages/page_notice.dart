import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/model/notice/notice_item.dart';
import 'package:dclost_app/pages/page_notice_detail.dart';
import 'package:flutter/material.dart';



class PageNotice extends StatefulWidget {
  const PageNotice({super.key});

  @override
  State<PageNotice> createState() => _PageNoticeState();
}

/// 공지사항 메인 페이지 입니다

// 공지사항 제목
class _PageNoticeState extends State<PageNotice> {
  static List<String> noticeName = [
    ' 설 연휴 배송 안내',
    ' 교환 반품 불가 안내사항',
    ' 배송 지연  안내 ',
    ' 입금 확인 안내',
    ' 3.1 배송 안내 ',
  ];

// 상세 페이지 이미지 주소

  static List<String> noticeImg =[
    'assets/delivery.png',
    'assets/return.png',
    'assets/delay.png',
    'assets/bank.png',
    'assets/notice_3.1.png',

  ];

// 공지사항 상세 페이지 내용

  static List<String> noticeText = [
    '설 연휴 배송 2/8일 출고 마감입니다 ! 자세한 내용 이미지 확인 부탁드려요 !',
    '교환 반품 불가 안내사항 입니다 ~!',
    '배송 지연 안내드립니다 !! ',
    '입금 확인 안내드립니다 !!',
    '3.1절 배송 안내 드립니다 !',
  ];

  final List<Notice> noticeData = List.generate(noticeName.length,
          (index) => Notice(noticeName[index], noticeImg[index], noticeText[index]));


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body:ListView.builder(
          itemCount: noticeData.length,
          itemBuilder: (context, index){
            return Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
              color: Colors.white,
              child: ListTile(
                  title: Text(noticeData[index].name),
                  // leading argument는 앱바에서 왼쪽에 아이콘 넣을때 사용했던 걔임

                  onTap: () {
                    // debugPrint(animalData[index].name);
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            PageNoticeDetail(notice: noticeData[index],)
                    )
                    );
                  }
              ),
            );
          }
      ),

    );
  }

}

