import 'package:flutter/material.dart';

/**
 * 아이디 찾기 페이지
 */

class LoginIdFind extends StatefulWidget {
  const LoginIdFind({super.key});

  @override
  State<LoginIdFind> createState() => _LoginIdFindState();
}

class _LoginIdFindState extends State<LoginIdFind> {
  final TextEditingController _usernameController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          children: [
            Text('아이디 찾기'),

            TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                labelText: '이름',
              ),
            ),
            Container(
              padding: EdgeInsets.all(35),
              child: Column(
                children: [
                  ElevatedButton(onPressed: () {},
                    style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                      backgroundColor: Colors.black,
                      foregroundColor: Colors.white,
                    ),
                    child: Text('휴대폰으로 인증하기'),),
                ],
              ),
            )
          ],
        ),

      ),

    );
  }
}