import 'package:dclost_app/components/component_text_btn.dart';
import 'package:dclost_app/config/config_form_validator.dart';
import 'package:dclost_app/functions/token_lib.dart';
import 'package:dclost_app/middleware/middleware_login_check.dart';
import 'package:dclost_app/model/login/login_request.dart';
import 'package:dclost_app/repository/repo_member.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);
  @override
  State<PageLogin> createState() => _PageLoginState();
}
class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoMember().doLogin(loginRequest).then((res) {
      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      debugPrint(err);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('로그인'),
      ),
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    return ListView(
      children: [
        const Text('회원 로그인이 필요합니다.'),
        Container(
          padding: const EdgeInsets.only(
            bottom: 10,
            left: 20,
            right: 20,
          ),
          child: FormBuilder(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  child: FormBuilderTextField(
                    name: 'username',
                    maxLength: 20,
                    keyboardType: TextInputType.text,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
                Container(
                  child: FormBuilderTextField(
                    obscureText: true,
                    name: 'password',
                    maxLength: 20,
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(errorText: formErrorRequired),
                      FormBuilderValidators.minLength(8, errorText: formErrorMinLength(8)),
                      FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                    ]),
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          child: ComponentTextBtn('로그인', () {
            // 폼 검사하는 것
            if(_formKey.currentState!.saveAndValidate()) {
              // 사람이 입력한 값을 모양으로 담아서 주는 것 ..
              LoginRequest loginRequest = LoginRequest(
                //값 넣구
                _formKey.currentState!.fields['username']!.value,
                _formKey.currentState!.fields['password']!.value,
              );
              _doLogin(loginRequest);
            }
          }),
        ),
      ],
    );
  }
}
