
import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/pages/page_subcription_complete.dart';
import 'package:flutter/material.dart';

enum PayWay { BANK, CARD }

class PageMembership extends StatefulWidget {
  const PageMembership({super.key});

  @override
  State<PageMembership> createState() => _PageMembershipState();
}

// 멤버십 가입 신청 페이지

class _PageMembershipState extends State<PageMembership> {
  PayWay _payWay = PayWay.BANK;
  List<String> _dropdownList = ['5일', '10일', '15일', '20일', '25일'];
  String _selectedDropdown = '5일';
  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child: Text(
                        '멤버십 가입 신청서',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Text(
                            '결제 수단 선택',
                        style: TextStyle(
                        fontSize: 20,
                        ),
                        ),
                        Row(
                          children: [
                            Container(
                              width: 200,
                              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: RadioListTile(
                                  title: Text(
                                      style:TextStyle(
                                        fontSize: 15
                                  ),
                                      "계좌 자동이체"),
                                  value: PayWay.BANK,
                                  groupValue: _payWay,
                                  onChanged: (PayWay? value){
                                    setState(() {
                                      _payWay = value!;
                                    });
                                  }),
                            ),
                            Container(
                              width: 200,
                              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                              child: RadioListTile(
                                  title: Text("카드 자동이체"),
                                  value: PayWay.CARD,
                                  groupValue: _payWay,
                                  onChanged: (PayWay? value){
                                    setState(() {
                                      _payWay = value!;
                                    });
                                  }),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Text(
                              '결제 정보',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                        Container(
                          child: Text('결제 은행(카드)와 번호를 (-) 제외하고 입력해주세요.'),
                        ),
                        Container(
                          width: 400,
                          child: TextField(
                            keyboardType: TextInputType.text,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Text(
                            '결제일 선택',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.fromLTRB(200, 0, 0, 0) ,
                              child: Text('매달'),
                            ),
                            Container(
                              margin: EdgeInsets.fromLTRB(20, 0, 0, 0),
                              alignment: Alignment.center,
                              child: DropdownButton(
                                value: _selectedDropdown,
                                items: _dropdownList.map((String item) {
                                  return DropdownMenuItem<String>
                                    (child: Text('$item'),
                                    value: item,
                                  );
                                }).toList(),
                                onChanged: (dynamic value){
                                  setState(() {
                                    _selectedDropdown = value;
                                  });
                                },
                              ),
                            ),
                          ],
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Text(
                            '자동이체 금액 안내',
                            style: TextStyle(
                              fontSize: 20,
                            ),
                          ),
                        ),
                        Container(
                          child: Image.asset('assets/membershipPrice.png'),
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Checkbox(value: _isChecked, onChanged: (bool? value){
                                setState(() {
                                  _isChecked = value!;
                                });
                              }),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text('자동이체를 위한 개인정보 수집에 동의합니다.')
                                ],
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.all(5),
                          child: Column(
                            children: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageSubcriptionComplete()));
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: _isChecked ? Colors.black : Colors.grey,
                                  foregroundColor: _isChecked ? Colors.white : Colors.black,
                                ),
                                child: Text('구독 신청'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
