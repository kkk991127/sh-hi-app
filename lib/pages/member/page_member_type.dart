import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:flutter/material.dart';

// 나의 이용 정보 .. 페이지 디자인은 생략함 시간 부족..
// 일단 데이터 목업으로

class MemberType extends StatefulWidget {
  const MemberType({
    super.key
  });

  @override
  State<MemberType> createState() => _MemberTypeState();
}

class _MemberTypeState extends State<MemberType> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body:
        Container(
            alignment: Alignment.center,
            margin: EdgeInsets.all(50),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children:[
                  Text(style: TextStyle(fontSize: 20), '내 이용 정보'),
                Container(
                  margin: EdgeInsets.all(10),
                  height: 4,
                  width: double.maxFinite,
                  color: Colors.black,

                  ),
                Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Container
                        (padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'이름 : 김미미')),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'연락처 : 010-0000-0000')),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'주소 : 경기도 안산시 단원구')),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'상세주소: 오피스텔')),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'구독서비스 : 멤버십 구독중')),
                      Container(
                          padding: EdgeInsets.all(10),
                          child: Text(style: TextStyle(fontSize: 15),'구독 자동이체 수단 : 계좌이체'))

                    ],
                  ),
                  ),
                ],
       ),
      ),
    );
  }
}
