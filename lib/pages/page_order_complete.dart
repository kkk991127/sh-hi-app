import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/pages/page_goods_list.dart';
import 'package:flutter/material.dart';

// 주문 완료하면 뜨는 페이지 !
class PageOrderComplete extends StatefulWidget {
  const PageOrderComplete({
    super.key
  });

  @override
  State<PageOrderComplete> createState() => _PageOrderCompleteState();
}

class _PageOrderCompleteState extends State<PageOrderComplete> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(0, 200, 0, 10),
              child: Text(
                '주문이 완료되었습니다.',
                style: TextStyle(
                  fontSize: 30,
                ),
              ),
            ),
            Container(
              child: Text(
                '주문해 주셔서 감사합니다 !',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
              child: Text(
                '디클로젯의 서비스를 더 구경해보세요 ~! ',
                style: TextStyle(
                  fontSize: 20,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(20, 10, 0, 0),
              alignment: Alignment.center,
              child: Column(
                children: [
                  Column(
                    children: [
                      Container(
                       padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          onPressed: () {
                          },
                          child: Text('매거진 보러 가기'),
                          style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                            backgroundColor: Colors.black,
                            foregroundColor: Colors.white,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsList()));
                          },
                          child: Text('카탈로그 더 보러 가기'),
                          style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                            backgroundColor: Colors.black,
                            foregroundColor: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),

    );
  }
}
