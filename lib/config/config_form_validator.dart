const String formErrorRequired = '이 값은 필수입니다.';
const String formErrorNumeric = '숫자만 입력해라.';
const String formErrorEmail = '올바른 이메일이 아니다.';
const String formErrorEqualPassword = '비밀번호 일치하지 않다.';

String formErrorMinNumber(int num) => '$num 이상 입력해주세요.';
String formErrorMaxNumber(int num) => '$num 이하로 입력해라.';
String formErrorMinLength(int num) => '$num 자 이상 입력해주세요.';
String formErrorMaxLength(int num) => '$num 자 이하로 입력해라.';