
import 'package:dclost_app/model/notice/notice_response.dart';

class NoticeDetailResult {
  String msg;
  num code;
  NoticeResponse data;

  NoticeDetailResult(this.msg,this.code,this.data);

  factory NoticeDetailResult.fromJson(Map<String,dynamic>json){
    return NoticeDetailResult(
        json['msg'],
        json['code'],
        NoticeResponse.fromJson(json['data']) // 맨위의 데이터로 바꿔줘
    );

  }
}