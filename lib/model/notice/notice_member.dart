class NoticeMember{

  num id;
  String username;
  String password;
  String passwordRe;
  String memberName;
  String birthDay;
  String phoneNumber;
  String memberAddress;
  String memberDetailedAddress;
  num postCode;
  bool nYPersonalInfo;
  bool nYMarketing;


  NoticeMember(this.id,this.username,this.memberName,this.password,this.passwordRe,
      this.birthDay,this.phoneNumber,this.memberAddress,this.memberDetailedAddress,
      this.postCode,this.nYPersonalInfo,this.nYMarketing);

  factory NoticeMember.fromJson(Map<String,dynamic> json){
    return NoticeMember(

        json['id'],
        json['username'],
        json['memberName'],
        json['password'],
        json['passwordRe'],
        json['birthDay'],
        json['phoneNumber'],
        json['memberAddress'],
        json['memberDetailedAddress'],
        json['postCode'],
        json['nYPersonalInfo'],
        json['nYMarketing'],
    );
  }
}