


import 'package:dclost_app/model/goods/goods_item.dart';

class GoodsListResult{

  String msg;
  num code;
  List<GoodsItem> list;
  num totalCount;

  GoodsListResult(this.msg,this.code,this.list,this.totalCount);

  factory GoodsListResult.fromJson(Map<String,dynamic> json) {
    return GoodsListResult(
        json['msg'],
        json['code'],
        json['list'] !=null ?
            (json['list'] as List).map((e) => GoodsItem.fromJson(e)).toList()
            : [],

        json['totalCount']
    );

  }
  //json으로 [{name:'ghd'},{name:'rla}]이런 String을 받아서 Object의 List로 바꾸고 난 후에
  //그럼 List니까 한뭉탱이씩 던져줄수있따.
  //근데 한 뭉탱이씩 던지겠다 하면서 한 뭉탱이 부르는거에 이름이 바로  e
  //작은 그릇으로 바꾸고 리스트 형태로 만든다



}